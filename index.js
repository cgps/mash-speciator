const fs = require('fs');
const tmp = require('tmp');

function parseMetadata(metadataFile) {
  return JSON.parse(fs.readFileSync(metadataFile, 'utf8'));
}

function createTmpFile(postfix, data) {
  return new Promise((resolve, reject) => {
    tmp.file({ postfix }, (err, filePath, fd, removeCallback) => {
      if (err) return reject(err);
      return fs.write(fd, data, writeErr => {
        if (writeErr) return reject(writeErr);
        return resolve({ filePath, removeCallback });
      });
    });
  });
}

function queryResults(match, metadata) {
  if (match === null) {
    return null;
  }

  const info = metadata.references[match.referenceIndex];
  if (info === null) {
    return null;
  }

  const refseqId = info[0];
  const taxId = info[1];

  const speciesId = metadata.organism[taxId];
  const [ speciesName, genusId ] = metadata.species[speciesId];
  
  const [ genusName, superkingdomId ] = metadata.genus[genusId];
  const superkingdomName = metadata.superkingdom[superkingdomId];

  return {
    refseqId,
    refseqTaxId: taxId,

    speciesTaxId: speciesId,
    speciesScientificName: speciesName,
    genusTaxId: genusId,
    genusScientificName: genusName,
    superkingdomTaxId: superkingdomId,
    superkingdomName: superkingdomName,

    mashDistance: match.mashDistance,
    pValue: match.pValue,
    matchingHashes: `${match.matchingHashes}/${match.allHashes}`,
  };
}

function queryFile(mashFunc, sketchFilePath, queryFilePath, metadata) {
  return (
    mashFunc(sketchFilePath, queryFilePath)
      .then(match => queryResults(match, metadata))
  );
}

function queryContents(mashFunc, sketchFilePath, contents, metadata) {
  return createTmpFile('query.fasta', contents)
    .then(({ filePath, removeCallback }) =>
      queryFile(mashFunc, sketchFilePath, filePath, metadata).then(mashResults => {
        removeCallback();
        return mashResults;
      })
    );
}

module.exports = function (sketchFilePath, metadataFilePath, masher = 'spawn') {
  const metadata = parseMetadata(metadataFilePath);
  const mashFunc = require(`./mashers/${masher}`);
  if (typeof mashFunc !== 'function') {
    throw new Error(`Invalid masher type '${masher}'`);
  }
  return {
    queryResults: results => queryResults(results, metadata),
    queryFile: queryFilePath => queryFile(mashFunc, sketchFilePath, queryFilePath, metadata),
    queryContents: queryFileContents => queryContents(mashFunc, sketchFilePath, queryFileContents, metadata),
  };
};
