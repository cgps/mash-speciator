/* eslint no-console: 0 */
/* eslint no-unused-vars: 0 */

const fs = require('fs');
const path = require('path');
const createMashSpecieator = require('./index');

const referencesDir = process.argv.length === 4 ? process.argv[2] : require('mash-sketches');
const referenceName = 'refseq-archaea-bacteria-fungi-viral-k16-s400';
const sketchFilePath = path.join(referencesDir, `${referenceName}.msh`);
const metadataFilePath = path.join(referencesDir, `${referenceName}.json`);

const mashNative = require('mash-node-native');

const specieator = createMashSpecieator(sketchFilePath, metadataFilePath);

const fastaFilePath = process.argv.length === 4 ? process.argv[3] : process.argv[2];

const { version } = require('./package.json');

console.log('Mash Specieator version', version);
console.log();
console.log('Using mash sketches in', referencesDir);
console.log('\t', 'sketch file      ', sketchFilePath.substr(referencesDir.length + 1));
console.log('\t', 'metadata file    ', metadataFilePath.substr(referencesDir.length + 1));
console.log();

function getTopMatch() {
  return specieator.queryFile(fastaFilePath);
}

function getAllMatches(n = 50) {
  const matches = mashNative.allMatchesSync(sketchFilePath, fastaFilePath);
  // const matches = (
  //   .map(([ refName, file, mashDistance, pValue, matchingHashes ], referenceIndex) => ({
  //     referenceIndex,
  //     mashDistance: parseFloat(mashDistance),
  //     pValue: parseFloat(pValue),
  //     matchingHashes: matchingHashes.split('/')[0],
  //     allHashes: matchingHashes.split('/')[1],
  //   }))
  // );
  const sorted = matches.sort((a, b) => {
    if (a.mashDistance !== b.mashDistance) {
      return a.mashDistance - b.mashDistance;
    } else if (a.pValue !== b.pValue) {
      return a.pValue - b.pValue;
    } else {
      return a.referenceIndex - b.referenceIndex;
    }
  });
  const topMatches = [];

  let index = 0;
  const dist = sorted[0].mashDistance;
  while (index < n || sorted[index].mashDistance === dist) {
    topMatches.push(sorted[index]);

    index += 1;
  }

  return topMatches;
}

function printCells(cells) {
  const vals = cells.map(
    ([ value, length ]) => `${value}${' '.repeat(Math.max(length, (value ? value.length : 0)) - (value ? value.length : 0))}`
  );
  console.log('', vals.join('| '));
}

function printAsTable(headerCells, rows) {
  printCells(headerCells);

  console.log(
    headerCells.map(([ , length ]) => '-'.repeat(length + 1))
    .join('|')
  );

  for (const row of rows) {
    printCells(
      row.map((cell, index) => [ cell, headerCells[index][1] ])
    );
  }
}

Promise.all([
  getTopMatch(),
  getAllMatches(),
])
.then(([ res, topMatches ]) => {
  console.log('Results using mash-node-native:');
  Object.keys(res).map(key => console.log('\t', key, ' '.repeat(24 - key.length), res[key]));
  console.log();
  // console.dir(Object.keys(res));

  printAsTable(
    [
      [ 'referenceId', 16 ],
      [ 'Distance', 24 ],
      [ 'P-Value', 24 ],
      [ 'Hashes', 8 ],
      [ 'Tax ID', 8 ],
      [ 'Species', 8 ],
      [ 'Scientific Name', 64 ],
    ],
    topMatches.map(match => {
      const result = specieator.queryResults(match);
      const { refseqId, refseqTaxId, speciesTaxId, speciesScientificName, mashDistance, pValue, matchingHashes } = result;
      return [
        refseqId.toString(),
        mashDistance.toString(),
        pValue.toString(),
        matchingHashes,
        refseqTaxId,
        speciesTaxId,
        speciesScientificName,
      ];
    })
  );
})
.catch(err => console.error(err));
