/* eslint no-param-reassign: 0 */

const exec = require('child_process').exec;

function closestMatchAsync(referenceFile, queryFile) {
  return new Promise((resolve, reject) => {
    try {
      const command =
        `node -e 'console.log(JSON.stringify(require("mash-node-native").closestMatchSync(process.argv[1], process.argv[2])));' '${referenceFile.replace(/'/g, "'\\''")}' '${queryFile.replace(/'/g, "'\\''")}'`;
      exec(command, (err, stdout, stderr) => {
        if (err) {
          stderr = null;
          reject(err, stderr);
        } else {
          stderr = null;
          resolve(JSON.parse(stdout));
        }
      });
    } catch (e) {
      reject(e);
    }
  });
}

module.exports = closestMatchAsync;
