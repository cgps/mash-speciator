const fs = require('fs');
const path = require('path');
const papaparse = require('papaparse');

const referencesDir = path.join(__dirname, '..', '..', 'references');
const referenceInfoFile = path.join(referencesDir, 'refseq-all-k16-s400.dist.csv');
const assemblySummaryFile = path.join(referencesDir, 'assembly_summary_refseq.txt');
const assemblySummaryPatchFile = path.join(referencesDir, 'assembly_summary_refseq-patch.csv');
const metadataFile = path.join(referencesDir, 'refseq-all-k16-s400.csv');

function parseCSV(csv, delimiter = '\t', newline = '\n') {
  return papaparse.parse(csv, {
    delimiter,
    newline,
    header: false,
    dynamicTyping: false,
    skipEmptyLines: true,
  });
}

function parseAssemblySummary() {
  // const results = parseCSV(fs.readFileSync(assemblySummaryFile, 'utf8'), ',', '\n');
  // return results.data;
  return fs.readFileSync(assemblySummaryFile, 'utf8')
          .split('\n')
          .map(row => row.split('\t'))
          .map(row => [ row[0], row[5], row[6], row[7] ]);
}

function parseAssemblySummaryPatch() {
  return fs.readFileSync(assemblySummaryPatchFile, 'utf8')
          .split('\n')
          .map(row => row.split(','));
}

function createLockupDictionary(sources) {
  const lookupDictionary = {};
  for (const rows of sources) {
    for (const row of rows) {
      lookupDictionary[row[0]] = row;
    }
  }
  return lookupDictionary;
}

function parseReferenceInfo() {
  const results = parseCSV(fs.readFileSync(referenceInfoFile, 'utf8'), '\t', '\n');
  return results.data.map(row => row[0]);
}

// function getRefseqMetadata(refseqMetadata, id) {
//   for (let i = 0; i < refseqMetadata.length; i++) {
//     const row = refseqMetadata[i];
//     if (row[0] === id) {
//       return [
//         row[0],
//         row[5],
//         row[6],
//         row[7],
//       ];
//     }
//   }
//   return null;
// }

function main() {
  const metadataRows = parseAssemblySummary();
  const additionalMetadataRows = parseAssemblySummaryPatch();

  const lookupDictionary = createLockupDictionary([
    metadataRows,
    additionalMetadataRows,
  ]);

  const referenceInfo = parseReferenceInfo();

  for (const id of referenceInfo) {
    const metadata = lookupDictionary[id];
    if (metadata) {
      console.log(metadata.join(','));
    } else {
      throw new Error(`Missing metadata '${id}'`);
    }
  }
}

main();
